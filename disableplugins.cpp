#include <stdio.h>
#include <dlfcn.h>
#include <QWebSettings>

typedef QWebSettings*(*func_t)();
QWebSettings *QWebSettings::globalSettings(){
  void *handle = NULL;
  const char library[] = "/usr/lib64/libQtWebKit.so.4.9.0";
  static func_t real_func = NULL;
  handle = dlopen(library, RTLD_LAZY);

  if (handle == NULL) {
    fprintf(stderr, "Failed to dlopen %s\n", library);
    fprintf(stderr, "dlerror says: %s\n", dlerror());
    return NULL;
  }
  real_func = (func_t)dlsym(handle, "_ZN12QWebSettings14globalSettingsEv");
  QWebSettings *s = real_func();
  s->setAttribute(QWebSettings::PluginsEnabled, false);
  return s;
}

